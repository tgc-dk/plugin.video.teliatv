# plugin.video.teliatv

Unofficial kodi addon for Telia TV (Denmark, Finland) and Telia Play (Sweden, Lithuania). Supports playback of Live TV,TV archive and Kids content. The Movieshop is not supported.

This addon is still under development so expect issues.

Note that Telia Play Norway and Telia TV Estonia is not supported, since those use a different platform.

## Using "PVR IPTV Simple Client" with Telia TV/Play

It is possible to get use Telia TV/Play as a EPG provider for Kodis "TV" feature, with catchup etc.

The "TV" feature of Kodi requires the use of a PVR. In our case we will use the PVR "IPTV Simple Client". IPTV Simple
Client will serve as a bridge between Kodi TV and the Telia TV/Play add-on.

First enable the EPG output generation in the Telia Tv/Play add-on:


1. Go to Telia Tv/Play settings and enable "Enable EPG data generation...". This will enable generation of two files: channels.m3u
   and epg.xml, which will be used by the IPTV Simple Client.
1. Optionally set a custom output folder, if no output folder is given the files will be placed in the data folder of the
Telia TV/Play add-on, the location of which depends on your system:
    - Windows: `C:\Users\[user]\AppData\Roaming\Kodi\userdata\addon_data\plugin.video.teliatv`
    - Linux: `/home/[user]/.kodi/userdata/addon_data/plugin.video.teliatv/`
    - macOS: `/Users/[user]/Library/Application Support/Kodi/userdata/addon_data/plugin.video.teliatv/`
    - Android: `/sdcard/Android/data/org.xbmc.kodi/files/.kodi/userdata/addon_data/plugin.video.teliatv/`
    - LibreELEC: `/storage/.kodi/userdata/addon_data/plugin.video.teliatv/`
    - Shield Android TV: `/internal/Android/data/org.xbmc.kodi/files/.kodi/userdata/addon_data/plugin.video.teliatv/`
    - Amazon Fire: `/External storage/Android/Data/org.xbmc.kodi/files/.kodi/userdata/addon_data/plugin.video.teliatv/`
1. Note the custom location or the system location relevant for you.
1. Restart Kodi for the EPG data generation to take effect.


Now for "PVR IPTV Simple Client". If Kodi is installed on Linux, you need to first install the PVR on your system with
the systems package manager. On Debian/Ubuntu based system you can use the command:
`sudo apt install kodi-pvr-iptvsimple`

Now to install "PVR IPTV Simple Client" in Kodi:
1. Open Kodi Settings.
1. Open "Add-ons".
1. Select "Install from repository".
1. Select "PVR clients".
1. Select "PVR IPTV Simple Client".
1. If it's not installed, install-it.

Configure "PVR IPTV Simple Client" in Kodi:
1. Open the settings for "PVR IPTV Simple Client".
1. In the "General" tab choose "Local path" for the location.
1. In the "General" tab choose "Local path" for the location.
1. Select "M3U Play List Path" to open the Kodi File Explorer.
1. Go to the location you note above as EPG output data folder (Note: hidden folders are not shown by default, can be
   changed in Settings > Media > General. Under Files, toggle the "Show hidden files and directories").
1. Select the channels.m3u file.
1. Now go to the "EPG Settings" tab of the PVR IPTV Simple Client settings and choose "Local path" for the location.
1. Select "XMLTV Path" to open the Kodi File Explorer.
1. Go to the location you note above as EPG output data folder (Note: hidden folders are not shown by default).
1. Select the epg.xml file.
1. Confirm the settings by pressing OK.
1. Restart Kodi.

The TV section of Kodi will now be active with channel list, EPG guide, etc. Enjoy!

