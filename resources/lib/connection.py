# -*- coding: utf-8 -*-
# Module: default
# Author: tgc
# Created on: 20.11.2020
# License: GPL v.3 https://www.gnu.org/copyleft/gpl.html

from __future__ import absolute_import, unicode_literals
from datetime import datetime, timedelta
import json
import re
import textwrap
import time
import uuid
import six
import requests
import xbmc
import xbmcgui
import xbmcplugin
# import from urllib depending on python version
try:
    # python 3 (kodi 19)
    from urllib.parse import unquote, quote_plus
except ImportError:
    # python 2 (kodi 18)
    from urllib import unquote, quote_plus

PROTOCOL = 'mpd'
DRM = 'com.widevine.alpha'
MIME_TYPE = 'application/dash+xml'
LICENSE_URL = 'https://wvls.webtv.telia.com:8063/'
# NOTE: The lists below must be in sync with the country_select setting lvalues
BASE_URLS = ['https://classic.teliatv.dk', 'https://classic.tv.telia.fi', 'https://classic.teliaplay.se', 'https://www.teliaplay.lt']
COUNTRY_CODES = ['dk', 'fi', 'se', 'lt']
IPTV_BASE_URL = ['https://iptvsearch-dk-playplus-prod.han.telia.se', 'https://iptvsearch-fi-playplus-prod.han.telia.se',
                 'https://iptvsearch-playplus-prod.han.telia.se', 'https://iptvsearch-lt-playplus-prod.han.telia.se']


def print_roundtrip(response, *args, **kwargs):
    def format_headers(d): '\n'.join('{k}: {v}'.format(k=k.encode('utf-8'), v=v.encode('utf-8')) for k, v in d.items())
    dump = textwrap.dedent('''
        ---------------- request ----------------
        {req.method} {req.url}
        {reqhdrs}

        {req.body}
        ---------------- response ----------------
        {res.status_code} {res.reason} {res.url}
        {reshdrs}

        {res.text}
    ''').format(req=response.request, res=response, reqhdrs=format_headers(response.request.headers),
                reshdrs=format_headers(response.headers))
    if six.PY2:
        xbmc.log(dump.encode('utf-8'), xbmc.LOGDEBUG)
    else:
        xbmc.log(dump, xbmc.LOGDEBUG)


class AuthenticationFailed(Exception):
    def __init__(self, msg, err_code=0):
        self.err_code = err_code
        self.msg = msg

    def __str__(self):
        return '{err}: {msg}'.format(err=self.err_code, msg=self.msg)


class TeliaConnection(object):
    """
    Handles the connection to Telia services, including authentication
    """
    def __init__(self, addon, handle):
        """
        Constructor, load values from settings
        """
        self.addon = addon
        self.handle = handle
        # load settings
        self.access_token = self.addon.getSettingString('access_token')
        self.refresh_token = self.addon.getSettingString('refresh_token')
        valid_to = self.addon.getSetting('token_valid_to')
        if valid_to:
            self.token_valid_to = datetime.fromtimestamp(int(valid_to))
        else:
            self.token_valid_to = datetime.now() - timedelta(days=1)
        session_cookie = self.addon.getSettingString('session_cookie')
        if session_cookie:
            self.session_cookie = requests.cookies.cookiejar_from_dict(json.loads(session_cookie))
        else:
            self.session_cookie = None
        self.user_id = self.addon.getSettingString('user_id')
        self.device_id = self.addon.getSettingString('device_id')
        # The device id should be fixed for the player, so only create on the first run
        if not self.device_id:
            self.device_id = 'DASHJS_' + str(uuid.uuid4())
            self.addon.setSettingString('device_id', self.device_id)
        xbmc.log(self.device_id, xbmc.LOGDEBUG)
        self.tv_client_boot_id = self.addon.getSettingString('tv_client_boot_id')
        my_live_channels = self.addon.getSettingString('my_live_channels')
        if my_live_channels:
            self.my_live_channels = json.loads(my_live_channels)
        else:
            self.my_live_channels = []
        my_play_channels = self.addon.getSettingString('my_play_channels')
        if my_play_channels:
            self.my_play_channels = json.loads(my_play_channels)
        else:
            self.my_play_channels = []
        self.country = int(self.addon.getSetting('country_select'))
        self.ask_for_live_restart = (self.addon.getSetting('ask_for_live_restart') == 'true')
        self.dump_traffic = (self.addon.getSetting('dump_traffic') == 'true')
        # Default versions, will be overwritten in get_versions() as part of authentication
        self.core_version = '3.35.3'
        self.ui_version = '6.24.7(610)'
        self.firefox_version = '92.0.1'
        # initialize to empty
        self.engagement_live_channels = []
        self.engagement_play_channels = []
        # create a request session used for the livetime of the object, which means execution of one command.
        self.session = requests.Session()
        # shorthand...
        self.lang = self.addon.getLocalizedString

    def save_settings(self):
        """
        Save settings so they can be restored later.
        """
        # device_id is saved at creation, no need to resave it
        self.addon.setSettingString('access_token', self.access_token)
        self.addon.setSettingString('refresh_token', self.refresh_token)
        timestamp = int(time.mktime(self.token_valid_to.timetuple()))
        self.addon.setSettingString('token_valid_to', str(timestamp))
        self.addon.setSettingString('session_cookie', json.dumps(self.session_cookie.get_dict()))
        self.addon.setSettingString('my_live_channels', json.dumps(self.my_live_channels))
        self.addon.setSettingString('my_play_channels', json.dumps(self.my_play_channels))
        self.addon.setSettingString('user_id', self.user_id)
        self.addon.setSettingString('tv_client_boot_id', self.tv_client_boot_id)

    def send_request(self, url, include_token=True, include_session_cookie=True, post_data=None, delete=False):
        """
        Send request to url
        :param url: The url to send the request to.
        :param include_token: Include the bearer/access token to the request, defaults to True.
        :param include_session_cookie: Include the session cookie to the request, defaults to True.
        :param post_data: If the request is a POST, include the data here, defaults to None.
        :param delete: Set the request type to delete, defaults to False.
        """
        http_headers = {
            'Connection': 'keep-alive',
            'Pragma': 'no-cache',
            'Cache-Control': 'no-cache',
            'tv-client-boot-id': self.tv_client_boot_id,
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:{v}) Gecko/20100101 Firefox/{v}'.format(v=self.firefox_version),
            'Accept': '*/*',
            'Origin': BASE_URLS[self.country],
            'Sec-Fetch-Site': 'cross-site',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Dest': 'empty',
            'Referer': BASE_URLS[self.country],
            'Accept-Language': 'en-US;q=0.9,en;q=0.8'
        }
        if post_data:
            http_headers['Content-Type'] = 'application/x-www-form-urlencoded'
        if include_session_cookie:
            self.session.cookies = self.session_cookie
            xbmc.log('setting cookie:' + str(self.session.cookies), xbmc.LOGDEBUG)
        else:
            self.session.cookie = None
        if include_token:
            http_headers['Authorization'] = 'Bearer ' + self.access_token
        # if enabled, dump network traffic to the debug log
        resp_hooks = None
        if self.dump_traffic:
            resp_hooks = {'response': print_roundtrip}
        if post_data is not None:
            r = self.session.post(url, headers=http_headers, data=post_data, hooks=resp_hooks)
        elif delete:
            r = self.session.delete(url, headers=http_headers, hooks=resp_hooks)
        else:
            r = self.session.get(url, headers=http_headers, hooks=resp_hooks)
        return r

    def get_versions(self):
        """
        Extracts the web platform versions for use in the REST calls
        """
        response = self.send_request(BASE_URLS[self.country], include_token=False, include_session_cookie=False)
        telia_html = response.text
        # get ui version
        m = re.search(r'window.tvegui.version = "(\d+.\d+.\d+\(\d+\))";', telia_html)
        if m:
            self.ui_version = m.group(1)
        # find js to get core version
        m = re.search(r'src="(src/core-\w+)/bootstrap.js"', telia_html)
        if m:
            js_url = m.group(1)
        else:
            # if url was not found, then stop here
            return
        full_js_url = '{base}/{js_url}/telia-core-fw/bundle.min.js'.format(base=BASE_URLS[self.country], js_url=js_url)
        response = self.send_request(full_js_url, include_token=False, include_session_cookie=False)
        telia_js = response.text
        m = re.search(r'define\("telia-core-fw/Version",\[\],function\(\){return"(\d+.\d+.\d+)"}\);', telia_js)
        if m:
            self.core_version = m.group(1)
        # get latest firefox version
        response = self.send_request('https://www.mozilla.org/en-US/firefox/releases/', include_token=False,
                                     include_session_cookie=False)
        ff_html = response.text
        m = re.search(r'data-latest-firefox="(\d+\.\d+\.*\d*)" data-esr-versions=', ff_html)
        if m:
            self.firefox_version = m.group(1)
        xbmc.log("ui version: %s, core version: %s, firefox version: %s" % (self.ui_version, self.core_version,
                                                                            self.firefox_version), xbmc.LOGDEBUG)

    def get_session_cookie(self):
        """
        Retrive a session cookie
        """
        url = '{base}/rest/secure/users/authentication?deviceId={deviceId}&coreVersion={coreVersion}'\
              '&model=desktop_linux&nativeVersion=unknown_nativeVersion&uiVersion={uiVersion}'
        expanded_url = url.format(base=BASE_URLS[self.country], deviceId=self.device_id, coreVersion=self.core_version,
                                  uiVersion=self.ui_version)
        response = self.send_request(expanded_url, include_token=False, include_session_cookie=False)
        self.session_cookie = response.history[0].cookies
        xbmc.log(str(self.session_cookie.get_dict()), xbmc.LOGDEBUG)

    def get_bearer_token(self):
        """
        Return bearer token
        """
        url = '{base}/rest/secure/users/authentication/j_security_check?deviceType=WEB'
        expanded_url = url.format(base=BASE_URLS[self.country])
        data = {'j_username': self.addon.getSettingString('username'),
                'j_password': self.addon.getSettingString('password')}
        response = self.send_request(expanded_url, include_token=False, include_session_cookie=False, post_data=data)
        xbmc.log('j_security_check: ' + response.text, xbmc.LOGDEBUG)
        json_response = json.loads(response.text)
        if json_response.get('errorCode', None) is not None:
            xbmc.log(str(json_response), xbmc.LOGERROR)
            raise AuthenticationFailed(json_response['errorMessage'], json_response['errorCode'])
        self.access_token = json_response['token']['accessToken']
        self.refresh_token = json_response['token']['refreshToken']
        valid_to = json_response['token']['validTo']
        date_time_format = '%Y-%m-%dT%H:%M:%S.%f+' + valid_to.split('+')[1]
        self.token_valid_to = datetime(*(time.strptime(valid_to, date_time_format)[0:6]))
        xbmc.log(str(self.token_valid_to), xbmc.LOGDEBUG)
        self.session_cookie = response.history[0].cookies
        xbmc.log(str(self.session_cookie.get_dict()), xbmc.LOGDEBUG)

    def get_live_channels(self):
        """
        """
        channel_url = 'https://ottapi.prod.telia.net/web/{cc}/contentsourcegateway/rest/v1/channels?deviceTypes=WEB'
        expanded_url = channel_url.format(cc=COUNTRY_CODES[self.country])
        response = self.send_request(expanded_url, include_token=False, include_session_cookie=False)
        channel_json = json.loads(response.text)
        self.my_live_channels = []
        for chan in channel_json:
            if int(chan['id']) in self.engagement_live_channels:
                self.my_live_channels.append(chan)
        #xbmc.log(self.my_live_channels, xbmc.LOGDEBUG)

    def get_play_channels(self):
        """
        """
        channel_url = '{base}/rest/v3/stores?deviceType=WEB&serviceType=SVOD&includeHiddenStores=true'\
                      '&includeParentalContent=true&includeTestStores=false&includeEmptyContent=true&sort=sortorder'
        response = self.send_request(channel_url.format(base=BASE_URLS[self.country]), include_token=False)
        channel_json = json.loads(response.text)
        self.my_play_channels = []
        for chan in channel_json['stores']:
            #xbmc.log(str(chan), xbmc.LOGDEBUG)
            if int(chan['id']) in self.engagement_play_channels:
                self.my_play_channels.append(chan)
        #xbmc.log(self.my_play_channels, xbmc.LOGDEBUG)

    def get_customer_engagement(self):
        """
        """
        url = '{base}/rest/v3/secure/engagements/customerengagement'.format(base=BASE_URLS[self.country])
        response = self.send_request(url)
        engagement_json = json.loads(response.text)
        xbmc.log(response.text, xbmc.LOGDEBUG)
        try:
            self.engagement_live_channels = engagement_json['liveChannels']
        except KeyError as k:
            xbmc.log(str(k), xbmc.LOGDEBUG)
            self.engagement_live_channels = []
        self.engagement_play_channels = []
        try:
            for chan in engagement_json['playChannels']:
                self.engagement_play_channels.append(chan['id'])
                xbmc.log(str(chan['id']), xbmc.LOGDEBUG)
        except KeyError as k:
            xbmc.log(str(k), xbmc.LOGDEBUG)
        self.user_id = engagement_json['username']

    def parse_date(self, date_str):
        """
        Parses a date string. Not the same as in TeliaTvAddon
        """
        date_time_format = '%Y-%m-%dT%H:%M:%S.%f+'
        ret_date = datetime(*(time.strptime(date_str, date_time_format)[0:6]))
        return ret_date

    def refresh_tokens(self):
        """
        Refresh the access token using the refresh token.
        """
        url = '{base}/rest/secure/token/{rtoken}'.format(base=BASE_URLS[self.country], rtoken=self.refresh_token)
        response = self.send_request(url, include_token=False, include_session_cookie=True)
        json_response = json.loads(response.text)
        xbmc.log(str(json_response), xbmc.LOGDEBUG)
        try:
            self.access_token = json_response['accessToken']
            self.refresh_token = json_response['refreshToken']
            valid_to = json_response['validTo']
        except KeyError:
            raise AuthenticationFailed(str(json_response))
        self.token_valid_to = self.parse_date(valid_to)
        xbmc.log(str(self.token_valid_to), xbmc.LOGDEBUG)

    def check_auth(self):
        """
        """
        xbmc.log('In check_auth', xbmc.LOGDEBUG)
        if not self.token_valid_to:
            self.token_valid_to = datetime.now() - timedelta(days=1)
        xbmc.log('token_valid_to: ' + str(self.token_valid_to), xbmc.LOGDEBUG)
        refresh_time_delta = self.token_valid_to - datetime.now()
        xbmc.log('refresh_time_delta: ' + str(refresh_time_delta), xbmc.LOGDEBUG)
        #if refresh_time_delta > timedelta(minutes=1) and refresh_time_delta < timedelta(minutes=15):
        #    xbmc.log('In check_auth - refreshing tokens', xbmc.LOGDEBUG)
        #    # use refresh token to get new access token
        #    self.refresh_tokens()
        if not self.access_token or refresh_time_delta < timedelta(minutes=1):
            xbmc.log('In check_auth - logging in', xbmc.LOGDEBUG)
            self.tv_client_boot_id = str(uuid.uuid4())
            self.get_versions()
            self.get_session_cookie()
            self.get_bearer_token()
            self.get_customer_engagement()
            self.get_live_channels()
            self.get_play_channels()
            self.save_settings()
        else:
            xbmc.log('In check_auth - token is still valid!', xbmc.LOGDEBUG)

    def play_item(self, channel, stream_type):
        """
        Start playback of a video
        """
        self.check_auth()
        # if of type channel, the input is the name of the channel so we need to find the associated id
        set_start_from_beginning = False
        if stream_type == 'CHANNEL':
            unquoted_channel = unquote(channel)
            channel = None
            for chan in self.my_live_channels:
                if unquoted_channel == chan['name']:
                    channel = chan['id']
                    break
            if not channel:
                return
            # if enabled and supported ask whether to play current live program from the beginning
            if self.ask_for_live_restart:
                program_json = self.get_epg(channel)
                if program_json and 'map' in program_json:
                    program_json = program_json['map']
                    if channel in program_json and program_json[channel]:
                        cur_chan_programs = program_json[channel][0]
                        if cur_chan_programs['serviceFlags']['catchUp'] and \
                                cur_chan_programs['serviceFlags']['startOver']:
                            msg = '%s "%s". %s' % (self.lang(30025), cur_chan_programs['title'], self.lang(30026))
                            ret = xbmcgui.Dialog().yesno(self.addon.getAddonInfo('name'), msg)
                            if ret:
                                channel = cur_chan_programs['assetId']
                                stream_type = 'CATCHUP'
                                set_start_from_beginning = True
        # get a playback ticket
        ticket_url = 'https://ottapi.prod.telia.net/web/{cc}/streaminggateway/rest/secure/v1/streamingticket/'\
                     '{stream_type}/{channel}/DASH?playerProfile=DEFAULT&sessionId={session_uuid}'
        session_uuid = six.text_type(uuid.uuid4())
        full_ticket_url = ticket_url.format(cc=COUNTRY_CODES[self.country], stream_type=stream_type, channel=channel,
                                            session_uuid=session_uuid)
        xbmc.log(full_ticket_url, xbmc.LOGDEBUG)
        first_time = datetime.now()
        resp = self.send_request(full_ticket_url, post_data='')
        if resp.status_code == 403:
            xbmcgui.Dialog().ok(self.addon.getAddonInfo('name'), self.lang(30016))
            return
        xbmc.log(str(resp.status_code), xbmc.LOGDEBUG)
        xbmc.log(resp.text, xbmc.LOGDEBUG)
        ticket_json = json.loads(resp.text)
        # Get streaming location
        current_time = int(ticket_json['currentTime'])
        second_time = datetime.now()
        delta = second_time - first_time
        delta_ms = delta.microseconds // 1000
        new_time = current_time + delta_ms
        raw_url = '{base}?ssl=true&time={new_time}&token={token}&expires={expires}&c={user_id}&d={dev_id}'
        streaming_url = raw_url.format(base=ticket_json['streamingUrl'], new_time=str(new_time),
                                       token=ticket_json['token'], expires=str(ticket_json['expires']),
                                       user_id=self.user_id, dev_id=self.device_id)
        resp = self.send_request(streaming_url, include_token=False)
        location_json = json.loads(resp.text)
        streaming_url = location_json['location']
        xbmc.log(streaming_url, xbmc.LOGDEBUG)
        from inputstreamhelper import Helper  # type: ignore
        protocol = PROTOCOL
        drm = DRM
        is_helper = Helper(protocol, drm=drm)
        if is_helper.check_inputstream():
            license_headers = 'User-Agent=Mozilla/5.0 (X11; Linux x86_64; rv:83.0) Gecko/20100101 Firefox/83.0'\
                              '&X-AxDRM-Message={dev_id}'
            license_headers = license_headers.format(dev_id=self.device_id)
            # Create a playable item with a path to play.
            play_item = xbmcgui.ListItem(path=streaming_url)
            play_item.setContentLookup(False)
            play_item.setMimeType(MIME_TYPE)
            if six.PY2:
                play_item.setProperty('inputstreamaddon', is_helper.inputstream_addon)
            else:
                play_item.setProperty('inputstream', is_helper.inputstream_addon)
            play_item.setProperty('inputstream.adaptive.manifest_type', PROTOCOL)
            play_item.setProperty('inputstream.adaptive.license_type', DRM)
            # Note: in the explanded key value 'R{{SSM}}' is 'R{SSM}'
            play_item.setProperty('inputstream.adaptive.license_key',
                                  '{url}|{headers}|R{{SSM}}|'.format(url=LICENSE_URL, headers=license_headers))
            play_item.setProperty('IsPlayable', 'true')
            xbmc.log('Setting the url', xbmc.LOGDEBUG)
            xbmcplugin.setResolvedUrl(handle=self.handle, succeeded=True, listitem=play_item)
            xbmc.log('setResolvedUrl done', xbmc.LOGDEBUG)
            xbmc.log('isPlaying: ' + str(xbmc.Player().isPlaying()), xbmc.LOGDEBUG)
            # Wait for player to start playing. This is not exactly bulletproof...
            xbmc.sleep(2000)
            while not xbmc.Player().isPlaying():
                xbmc.sleep(100)
            # if we want to play a live program from the beginning we must set it here.
            # Setting the StartOffset property on play_item does not work
            xbmc.sleep(200)
            if set_start_from_beginning:
                xbmc.Player().seekTime(0.0)
            # we need to send a delete request to close the session after playback stops
            while xbmc.Player().isPlaying():
                xbmc.sleep(300)
                #xbmc.log('looping while playing', xbmc.LOGDEBUG)
            delete_url = 'https://ottapi.prod.telia.net/web/{cc}/streaminggateway/rest/secure/v1/streamingticket/'\
                         '{stream_type}/{channel}?sessionId={session_uuid}'
            full_delete_url = delete_url.format(cc=COUNTRY_CODES[self.country], stream_type=stream_type,
                                                channel=channel, session_uuid=session_uuid)
            resp = self.send_request(full_delete_url, delete=True)
            xbmc.log('delete status code: ' + str(resp.status_code), xbmc.LOGDEBUG)
        else:
            xbmcgui.Dialog().ok(self.addon.getAddonInfo('name'), self.lang(30027))

    def get_play_catagory(self, play_service_id, cat_id, from_idx=0, to_idx=40):
        """
        Get titles from a category
        """
        url = '{base}/v1/categories/{cat_id}/titles?sort=valid_from&format=dash&parental=true'\
              '&fromIndex={f}&toIndex={t}&storeId={store_id}'
        expanded_url = url.format(base=IPTV_BASE_URL[self.country], cat_id=cat_id, store_id=play_service_id, f=from_idx,
                                  t=to_idx)
        response = self.send_request(expanded_url, include_token=False, include_session_cookie=False)
        cat_json = json.loads(response.text)
        return cat_json

    def get_user_favorites(self):
        """
        Get the favorites of the user.
        """
        self.check_auth()
        # hardcoded to 40 entries
        url = 'https://ottapi.prod.telia.net/web/{cc}/mylistgateway/rest/secure/v1/savedItems/list?protocols=dash'\
              '&resolutions=sd&fromIndex=0&toIndex=40'
        response = self.send_request(url.format(cc=COUNTRY_CODES[self.country]), include_token=True,
                                     include_session_cookie=False)
        fav_items = json.loads(response.text)
        fav_ids = []
        for item in fav_items['items']:
            fav_ids.append(item['id'])
        # stop if no favorites
        if not fav_ids:
            return ''
        url_fav_details = 'https://ottapi.prod.telia.net/web/{cc}/exploregateway/rest/v3/explore/media/{ids}'\
                          '?deviceType=WEB&protocols=DASH'
        response = self.send_request(url_fav_details.format(cc=COUNTRY_CODES[self.country], ids=','.join(fav_ids)),
                                     include_token=False, include_session_cookie=False)
        fav_details = json.loads(response.text)
        # TODO: this only handles series, what about movies?
        series_id = []
        for fav in fav_details:
            serie = True
            try:
                serie_id = fav['references']['loopSeriesId']
                series_id.append(serie_id)
            except KeyError:
                serie = False
            if not serie:
                try:
                    serie_id = fav['references']['loopId']
                    series_id.append(serie_id)
                except KeyError:
                    pass
        return ','.join(series_id)

    def get_play_item_list(self, play_id, from_idx=0, to_idx=40):
        """
        Get list of titles available from a given store
        """
        url = '{base}/v1/stores/{id}/titles?sort=alphabetic&format=dash&parental=true&fromIndex={f}&toIndex={t}'
        expanded_url = url.format(base=IPTV_BASE_URL[self.country], id=play_id, f=from_idx, t=to_idx)
        response = self.send_request(expanded_url, include_token=False, include_session_cookie=False)
        json_response = json.loads(response.text)
        items = json_response
        return items

    def get_play_catagories(self, play_service_id):
        """
        Get list of categories available from a given store
        """
        url = '{base}/rest/v2/stores/{play_service_id}?deviceType=WEB&includeParentalContent=true'
        expanded_url = url.format(base=BASE_URLS[self.country], play_service_id=play_service_id)
        response = self.send_request(expanded_url, include_token=False, include_session_cookie=False)
        cat_json = json.loads(response.text)
        xbmc.log(str(cat_json), xbmc.LOGDEBUG)
        return cat_json

    def get_season(self, series_id, season_id):
        """
        Get list of episoes in a season
        """
        url = '{base}/v1/series/{series_id}/season/{season_id}/episodes?sort=episode_asc&format=dash&parental=true'\
              '&fromIndex=0&toIndex=5000'
        response = self.send_request(url.format(base=IPTV_BASE_URL[self.country], series_id=series_id,
                                                season_id=season_id), include_token=False, include_session_cookie=False)
        season_json = json.loads(response.text)
        return season_json

    def get_suggested_cat_content(self, ids):
        """
        Get the suggested categories (list of ids) that is not a link to a store
        """
        url = '{base}/v1/videos?seriesIds={ids}&oneCoverIds={ids}&format=dash&parental=true&toIndex=35'
        expanded_url = url.format(base=IPTV_BASE_URL[self.country], ids=ids)
        response = self.send_request(expanded_url, include_token=False, include_session_cookie=False)
        cat_json = json.loads(response.text)
        return cat_json

    def get_kids_categories(self):
        """
        Get list of kids categories as seen on the webpage
        """
        self.check_auth()
        url = 'https://ottapi.prod.telia.net/web/{cc}/discoverygateway/rest/secure/v2/discovery/applications/'\
              'play-library-kids/?deviceType=WEB&protocols=DASH&resolutions=SD&parentalContent=false'
        expanded_url = url.format(cc=COUNTRY_CODES[self.country])
        response = self.send_request(expanded_url, include_session_cookie=False)
        kids_json = json.loads(response.text)
        return kids_json

    def get_frontpage_categories(self):
        """
        Get list of categories as seen on the frontpage
        """
        self.check_auth()
        # get suggested categories
        url = 'https://ottapi.prod.telia.net/web/{cc}/discoverygateway/rest/secure/v2/discovery/applications/'\
              'welcome-page/?deviceType=WEB&protocols=DASH&resolutions=SD&parentalContent=false'
        expanded_url = url.format(cc=COUNTRY_CODES[self.country])
        response = self.send_request(expanded_url, include_session_cookie=False)
        welcome_json = json.loads(response.text)
        return welcome_json

    def get_search_result(self, search_query):
        """
        Get search result
        """
        self.check_auth()
        url = '{base}/v1/search?format=dash_sd&fromIndex=0&liveId={liveids}&parental=true&playId={playids}&'\
              'search={query}&toIndex=50'
        liveids = [lc['id'] for lc in self.my_live_channels]
        playids = [pc['id'] for pc in self.my_play_channels]
        query = quote_plus(search_query)
        expanded_url = url.format(base=IPTV_BASE_URL[self.country], liveids=','.join(liveids),
                                  playids=','.join(playids), query=query)
        response = self.send_request(expanded_url, include_session_cookie=False)
        search_json = json.loads(response.text)
        return search_json

    def get_epg(self, channels=None, from_time=None, to_time=None):
        """
        Return the current showing programs on live channels
        """
        url = '{base}/rest/v2/epg/{chans}/map?deviceType=WEB&fromTime={start}&toTime={end}&followingPrograms=0'
        if channels:
            channel_ids = channels
        else:
            liveids = [lc['id'] for lc in self.my_live_channels]
            channel_ids = ','.join(liveids)
        if from_time is None:
            from_time = int(time.time()) * 1000
        if to_time is None:
            to_time = int(time.time()) * 1000
        expanded_url = url.format(base=BASE_URLS[self.country], chans=channel_ids, start=from_time, end=to_time)
        response = self.send_request(expanded_url, include_token=False, include_session_cookie=False)
        programs_json = json.loads(response.text)
        return programs_json
