# -*- coding: utf-8 -*-
# Module: default
# Author: tgc
# Created on: 20.11.2020
# License: GPL v.3 https://www.gnu.org/copyleft/gpl.html

from __future__ import absolute_import, unicode_literals
import io
import time
from datetime import datetime, timedelta
from xml.etree.ElementTree import Element, ElementTree, SubElement
import six
import xbmcaddon
import xbmc

from connection import TeliaConnection

# import from urllib depending on python version
try:
    # python 3 (kodi 19)
    from urllib.parse import quote
except ImportError:
    # python 2 (kodi 18)
    from urllib import quote

M3U_EXTINF_TEMPLATE = '#EXTINF:0 tvg-id="{cid}" catchup="vod" tvg-logo="http://tve-icons.han.telia.se/app/mh512b/ch_{cid}.png",{chan_name}\n'
M3U_URL_TEMPLATE = 'plugin://plugin.video.teliatv/?action=play_channel&channel={chan_name}\n'
CATCHUP_URL = 'plugin://plugin.video.teliatv/?action=play_video&amp;play_item={item}'
SECONDS_PER_DAY = 60*60*24
EPG_GENERATE_INTERVAL_HOURS = 12


class TeliaEpgService(object):
    def __init__(self):
        """
        Constructor.
        """
        self.addon = xbmcaddon.Addon('plugin.video.teliatv')
        self.con = None

    def generate_epg(self):
        return self.addon.getSetting('epg_service') == 'true'

    def setup_connection(self):
        self.con = TeliaConnection(self.addon, None)

    def close_connection(self):
        self.con = None

    def get_last_epg_created(self):
        """
        """
        last_epg_created_str = self.addon.getSetting('last_epg_created')
        if last_epg_created_str:
            return datetime.fromtimestamp(int(last_epg_created_str))
        return datetime.fromtimestamp(0)

    def get_chan_info(self, chan_id):
        """
        """
        for chan in self.con.my_live_channels:
            if chan['id'] == chan_id:
                return chan
        return None

    def create_files(self):
        # verify that the initial login has actually taken place by checking if any live channels exists
        if not self.con.my_live_channels:
            xbmc.log('Could not find any live-channels, no EPG will be created.', level=xbmc.LOGDEBUG)
            return False
        # fetch epg
        cur_time = int(time.time())
        from_time = (cur_time - SECONDS_PER_DAY) * 1000
        to_time = (cur_time + 2*SECONDS_PER_DAY) * 1000
        # Setting channels to None will fetch epg for all channels available for the account
        epg_json = self.con.get_epg(channels=None, from_time=from_time, to_time=to_time)
        channels = epg_json['map']

        # Create the root element
        tv_root = Element('tv')
        # Make a new document tree
        doc = ElementTree(tv_root)

        m3u = '#EXTM3U\n'

        # loop over channels in EPG
        for chan_id in channels:
            # Get channel info
            chan_info = self.get_chan_info(chan_id)
            if not chan_info:
                continue
            # create m3u entry for channel
            m3u += M3U_EXTINF_TEMPLATE.format(cid=chan_id, chan_name=chan_info['name'])
            m3u += M3U_URL_TEMPLATE.format(chan_name=quote(chan_info['name'].encode('utf_8')))

            chan_programs = channels[chan_id]
            # Create XMLTV entry for this channel
            channel_tag = SubElement(tv_root, 'channel', id=chan_id)
            SubElement(channel_tag, 'display-name').text = chan_info['name']
            # Create XMLTV programme entries for this channel
            for program in chan_programs:
                # Add the subelements
                start_time = time.localtime(int(program['startTime']) // 1000)
                end_time = time.localtime(int(program['endTime']) // 1000)
                start_str = time.strftime('%Y%m%d%H%M%S', start_time)
                stop_str = time.strftime('%Y%m%d%H%M%S', end_time)
                # NOTE: this shamelessly assumes we are located in a timezone that are "+" (east of GMT)
                start_str += ' +%04d' % (-time.timezone/36)
                stop_str += ' +%04d' % (-time.timezone/36)
                programme = SubElement(tv_root, 'programme', channel=chan_id, start=start_str, stop=stop_str)
                if program['serviceFlags']['catchUp'] and program['serviceFlags']['startOver']:
                    programme.set('catchup-id', CATCHUP_URL.format(item=program['assetId']))
                if 'title' in program:
                    title = SubElement(programme, 'title').text = program['title']
                if 'description' in program:
                    SubElement(programme, 'desc').text = program['description']
        # determine output folder
        epg_output_folder = self.addon.getSetting('epg_output_folder')
        if not epg_output_folder:
            if six.PY2:
                epg_output_folder = xbmc.translatePath(self.addon.getAddonInfo('profile')).decode('utf-8')
            else:
                epg_output_folder = xbmc.translatePath(self.addon.getAddonInfo('profile'))
        # Save to XML file
        xml_file_path = epg_output_folder + 'epg.xml'
        xbmc.log('Telia EPG generator will save xml at: %s' % xml_file_path, level=xbmc.LOGDEBUG)
        out_file = io.open(xml_file_path, 'wb')
        doc.write(out_file, xml_declaration=True, encoding='utf-8')
        out_file.close()
        # Save to m3u file
        m3u_file_path = epg_output_folder + 'channels.m3u'
        out_file = io.open(m3u_file_path, 'wt', encoding='utf-8')
        out_file.write(m3u)
        out_file.close()
        # save time of EPG generation
        timestamp = int(time.mktime(datetime.now().timetuple()))
        self.addon.setSettingString('last_epg_created', str(timestamp))
        return True


if __name__ == '__main__':
    monitor = xbmc.Monitor()
    epg_service = TeliaEpgService()
    while not monitor.abortRequested() and epg_service.generate_epg():
        # Sleep until next EPG update, or break on kodi abort
        last_epg_created = epg_service.get_last_epg_created()
        next_update = last_epg_created + timedelta(hours=EPG_GENERATE_INTERVAL_HOURS)
        next_update_sec = (next_update - datetime.now()).total_seconds()
        if next_update_sec < 5:
            next_update_sec = 1
        xbmc.log("Telia EPG generator will sleep for %d seconds" % next_update_sec, level=xbmc.LOGDEBUG)
        if monitor.waitForAbort(next_update_sec):
            # Abort was requested while waiting. We should exit
            break
        xbmc.log("About to generate Telia EPG! %s" % time.time(), level=xbmc.LOGDEBUG)
        epg_service.setup_connection()
        if not epg_service.create_files():
            # Something was wrong: stop
            break
        epg_service.close_connection()
