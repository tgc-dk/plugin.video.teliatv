# -*- coding: utf-8 -*-
# Module: default
# Author: tgc
# Created on: 20.11.2020
# License: GPL v.3 https://www.gnu.org/copyleft/gpl.html

from __future__ import absolute_import
import sys

from teliatv import TeliaTvAddon


if __name__ == '__main__':
    # Get the plugin url in plugin:// notation.
    _url = sys.argv[0]
    # Get the plugin handle as an integer number.
    handle = int(sys.argv[1])
    telia_addon = TeliaTvAddon(handle)
    # Call the router function and pass the plugin call parameters to it.
    # We use string slicing to trim the leading '?' from the plugin call paramstring
    telia_addon.router(sys.argv[2][1:])
