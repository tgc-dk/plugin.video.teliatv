# -*- coding: utf-8 -*-
# Module: default
# Author: tgc
# Created on: 20.11.2020
# License: GPL v.3 https://www.gnu.org/copyleft/gpl.html

from __future__ import absolute_import, unicode_literals
from datetime import datetime
import time
import xbmc
import xbmcgui
import xbmcplugin
import xbmcaddon

from connection import TeliaConnection, AuthenticationFailed

# import from urllib depending on python version
try:
    # python 3 (kodi 19)
    from urllib.parse import unquote, quote, parse_qsl
except ImportError:
    # python 2 (kodi 18)
    from urllib import unquote, quote
    from urlparse import parse_qsl

BASE_PLUGIN_URL = 'plugin://plugin.video.teliatv/'
IMAGE_BASE_URL = ['http://dk-iptvlogin.telia.se/', 'http://fi-iptvlogin.telia.se/', 'http://iptvlogin.telia.se/',
                  'https://ottapi.prod.telia.net/custapp_ott_lt/']
PAGE_SIZE = 40


class TeliaTvAddon(object):
    def __init__(self, handle):
        """
        Constructor. Loads settings.
        """
        self.addon = xbmcaddon.Addon()
        self.handle = handle
        self.con = TeliaConnection(self.addon, self.handle)
        # shorthand...
        self.lang = self.addon.getLocalizedString

    def parse_date(self, date_str):
        """
        Parses a date string. Not the same as in TeliaConnection
        """
        date_time_format = '%Y-%m-%dT%H:%M:%S+' + date_str.split('+')[1]
        ret_date = datetime(*(time.strptime(date_str, date_time_format)[0:6]))
        return ret_date

    def list_live_channels(self):
        """
        Create the list of playable channels.
        :param category: Category name
        :type category: str
        """
        self.con.check_auth()
        # Set plugin category. It is displayed in some skins as the name
        # of the current section.
        xbmcplugin.setPluginCategory(self.handle, self.lang(30009))
        # Set plugin content. It allows Kodi to select appropriate views
        # for this type of content.
        xbmcplugin.setContent(self.handle, 'videos')
        # Get "tv guide"
        programs = self.con.get_epg()
        if programs and 'map' in programs:
            programs = programs['map']
        # Iterate through videos.
        for video in self.con.my_live_channels:
            title = video['name']
            # look up the channel in the programlist (tvguide) and list the current program and mark with "*" if
            # playback from program beginning is supported.
            if video['id'] in programs and programs[video['id']]:
                cur_chan_programs = programs[video['id']][0]
                title += ' (' + cur_chan_programs['title'] + ')'
                if cur_chan_programs['serviceFlags']['catchUp'] and cur_chan_programs['serviceFlags']['startOver'] \
                        and self.con.ask_for_live_restart:
                    title += '*'
            # Create a list item with a text label and a thumbnail image.
            list_item = xbmcgui.ListItem(label=title)
            # Set additional info for the list item.
            # 'mediatype' is needed for skin to display info for this ListItem correctly.
            list_item.setInfo('video', {'title': title,
                                        'genre': 'LiveTV',
                                        'mediatype': 'video'})
            # Set graphics (thumbnail, fanart, banner, poster, landscape etc.) for the list item.
            # Here we use the same image for all items for simplicity's sake.
            # In a real-life plugin you need to set each image accordingly.
            icon_url = 'http://tve-icons.han.telia.se/app/mh512b/ch_{channel}.png'.format(channel=video['id'])
            list_item.setArt({'thumb': icon_url, 'icon': icon_url, 'fanart': icon_url})
            # Set 'IsPlayable' property to 'true'.
            # This is mandatory for playable items!
            list_item.setProperty('IsPlayable', 'true')
            # Create a URL for a plugin recursive call. Insert random id to prevent kodi from remembering
            url = '{base}?action=play_channel&channel={cid}&randomid={rid}'
            expanded_url = url.format(base=BASE_PLUGIN_URL, cid=quote(video['name'].encode('utf_8')),
                                      rid=int(time.time()))
            # Add the list item to a virtual Kodi folder.
            # is_folder = False means that this item won't open any sub-list.
            is_folder = False
            # Add our item to the Kodi virtual folder listing.
            xbmcplugin.addDirectoryItem(self.handle, expanded_url, list_item, is_folder)
        # Add a sort method for the virtual folder items (alphabetically, ignore articles)
        xbmcplugin.addSortMethod(self.handle, xbmcplugin.SORT_METHOD_LABEL_IGNORE_THE)
        # Finish creating a virtual folder.
        xbmcplugin.endOfDirectory(self.handle)

    def list_result(self, items, next_page_url=None, page_number=0):
        """
        Create a list based on the items json
        """
        for item in items:
            obj = item['object']
            obj_type = item['type']
            title = obj['title']
            # find price if any exists
            try:
                price = obj['assets'][0]['price']
                cur = obj['currency']
                if price > 0:
                    price_str = ' ({price} {cur})'.format(price=price, cur=cur)
                else:
                    price_str = ''
            except (KeyError, IndexError):
                price_str = ''
            if obj_type == 'SeriesObject':
                season_count = obj.get('totalSeasons', None)
                if season_count and season_count > 1:
                    seasons = ' ({e} seasons)'.format(e=season_count)
                    title += seasons
                else:
                    episode_count = obj.get('totalEpisodes', None)
                    xbmc.log('%d' % episode_count, xbmc.LOGDEBUG)
                    if episode_count:
                        episodes = ' ({e} episodes)'.format(e=episode_count)
                        title += episodes
            elif not price_str:
                # if a movie and not movieshop item, list when it as played
                starttime = self.parse_date(obj['start'])
                title += ' ({date})'.format(date=str(starttime))
            else:
                # insert prrice
                title += price_str
            #xbmc.log(title, xbmc.LOGDEBUG)
            # Create a list item with a text label and a thumbnail image.
            list_item = xbmcgui.ListItem(label=title)
            # Set additional info for the list item.
            # 'mediatype' is needed for skin to display info for this ListItem correctly.
            plot = obj.get('description_long', None)
            list_item.setInfo('video', {'title': title,
                                        'mediatype': 'video',
                                        'plot': plot})
            # Set graphics (thumbnail, fanart, banner, poster, landscape etc.) for the list item.
            # Here we use the same image for all items for simplicity's sake.
            # In a real-life plugin you need to set each image accordingly.
            icon_url = '{base}{image_url}'.format(base=IMAGE_BASE_URL[self.con.country],
                                                  image_url=obj['portraitImages'][0])
            list_item.setArt({'thumb': icon_url, 'icon': icon_url, 'fanart': icon_url})
            # Set 'IsPlayable' property to 'true'. This is mandatory for playable items!
            list_item.setProperty('IsPlayable', 'true')
            # Create a URL for a plugin recursive call.
            if obj_type == 'SeriesObject':
                is_folder = True
                seasons_list = ''
                for season in obj['seasons']:
                    seasons_list += season['loopId'] + ','
                seasons_list = seasons_list.strip(',')
                url = '{base}?action=list_seasons&play_item={item}&seasons={seasons}'
                expanded_url = url.format(base=BASE_PLUGIN_URL, item=obj['loopId'], seasons=seasons_list)
            else:
                is_folder = False
                url = '{base}?action=play_video&play_item={item}'
                expanded_url = url.format(base=BASE_PLUGIN_URL, item=obj['assets'][0]['id'])
            # Add the list item to a virtual Kodi folder.
            # is_folder = False means that this item won't open any sub-list.
            # Add our item to the Kodi virtual folder listing.
            xbmcplugin.addDirectoryItem(self.handle, expanded_url, list_item, is_folder)
        # create an entry to go to next page if possible
        if next_page_url:
            label = '{label} ({num})'.format(label=self.lang(30021), num=page_number)
            list_item = xbmcgui.ListItem(label=label)
            is_folder = True
            xbmcplugin.addDirectoryItem(self.handle, next_page_url, list_item, is_folder)

    def list_favorites(self):
        """
        List the favorites of the user.
        """
        series_ids = self.con.get_user_favorites()
        self.list_suggested_cat_content(series_ids, self.lang(30013))

    def list_play_catagory(self, store_id, cat_id, from_idx, to_idx):
        """
        Create the list of playable channels.
        :param category: Category name
        :type category: str
        """
        # Set plugin category. It is displayed in some skins as the name of the current section.
        xbmcplugin.setPluginCategory(self.handle, 'Play')
        # Set plugin content. It allows Kodi to select appropriate views for this type of content.
        xbmcplugin.setContent(self.handle, 'videos')
        # Get the list of videos in the category.
        items = self.con.get_play_catagory(store_id, cat_id, from_idx, to_idx)
        old_to_idx = int(to_idx)
        # detect if more pages are needed
        next_page_num = 0
        if old_to_idx < items['Data']['totalHits']:
            new_to_idx = old_to_idx + PAGE_SIZE
            url = '{base}?action=list_play_cat&cat_id={cat}&store_id={store}&from_idx={f}&to_idx={t}'
            next_page_url = url.format(base=BASE_PLUGIN_URL, cat=cat_id, store=store_id, f=old_to_idx, t=new_to_idx)
            next_page_num = new_to_idx // PAGE_SIZE
        else:
            next_page_url = None
        # Iterate through videos.
        self.list_result(items['Data']['directHits'], next_page_url, next_page_num)
        # Finish creating a virtual folder.
        xbmcplugin.endOfDirectory(self.handle)

    def list_season(self, play_item, season):
        """
        """
        season_json = self.con.get_season(play_item, season)
        # Set plugin category. It is displayed in some skins as the name of the current section.
        season_number = season.split('_')[1]
        season_label = '{season} {num}'.format(season=self.lang(30015), num=season_number)
        xbmcplugin.setPluginCategory(self.handle, season_label)
        # Set plugin content. It allows Kodi to select appropriate views for this type of content.
        xbmcplugin.setContent(self.handle, 'videos')
        self.list_result(season_json['Data']['directHits'])
        # Add a sort method for the virtual folder items (alphabetically, ignore articles)
        xbmcplugin.addSortMethod(self.handle, xbmcplugin.SORT_METHOD_LABEL_IGNORE_THE)
        # Finish creating a virtual folder.
        xbmcplugin.endOfDirectory(self.handle)

    def list_seasons(self, play_item, seasons):
        """
        """
        if not seasons:
            return
        xbmc.log(seasons, xbmc.LOGDEBUG)
        season_list = seasons.split(',')
        # if only one season is available, just display it
        if len(season_list) == 1:
            self.list_season(play_item, season_list[0])
            return
        xbmcplugin.setPluginCategory(self.handle, self.lang(30014))
        # Set plugin content. It allows Kodi to select appropriate views for this type of content.
        xbmcplugin.setContent(self.handle, 'videos')
        for season_id in season_list:
            season_number = season_id.split('_')[1]
            season_label = '{season} {num}'.format(season=self.lang(30015), num=season_number)
            list_item = xbmcgui.ListItem(label=season_label)
            is_folder = True
            url = '{base}?action=list_season&play_item={item}&season={season}'.format(base=BASE_PLUGIN_URL,
                                                                                      item=play_item, season=season_id)
            xbmcplugin.addDirectoryItem(self.handle, url, list_item, is_folder)
        # Add a sort method for the virtual folder items (alphabetically, ignore articles)
        xbmcplugin.addSortMethod(self.handle, xbmcplugin.SORT_METHOD_LABEL_IGNORE_THE)
        # Finish creating a virtual folder.
        xbmcplugin.endOfDirectory(self.handle)

    def list_play_items(self, play_service_id, from_idx, to_idx):
        """
        Create the list categories for a service
        """
        self.con.check_auth()
        # get catagories
        categories = self.con.get_play_catagories(play_service_id)
        # Set plugin category. It is displayed in some skins as the name of the current section.
        xbmcplugin.setPluginCategory(self.handle, categories['name'])
        # Set plugin content. It allows Kodi to select appropriate views for this type of content.
        xbmcplugin.setContent(self.handle, 'videos')
        if categories['categories']:
            for cat in categories['categories']:
                list_item = xbmcgui.ListItem(label=cat['name'])
                is_folder = True
                url = '{base}?action=list_play_cat&cat_id={cat}&store_id={store}&from_idx=0&to_idx={ps}'
                expanded_url = url.format(base=BASE_PLUGIN_URL, cat=cat['id'], store=play_service_id, ps=PAGE_SIZE)
                xbmcplugin.addDirectoryItem(self.handle, expanded_url, list_item, is_folder)
        else:
            # Iterate through videos.
            items = self.con.get_play_item_list(play_service_id, from_idx, to_idx)
            old_to_idx = int(to_idx)
            # detect if more pages are needed
            next_page_num = 0
            if old_to_idx < items['Data']['totalHits']:
                new_to_idx = old_to_idx + PAGE_SIZE
                url = '{base}?action=list_play_item&play_item={item}&from_idx={f}&to_idx={t}'
                next_page_url = url.format(base=BASE_PLUGIN_URL, item=play_service_id, f=old_to_idx, t=new_to_idx)
                next_page_num = new_to_idx // PAGE_SIZE
            else:
                next_page_url = None
            self.list_result(items['Data']['directHits'], next_page_url, next_page_num)
        # Finish creating a virtual folder.
        xbmcplugin.endOfDirectory(self.handle)

    def list_play_channels(self):
        """
        Create the list of playable channels.
        """
        self.con.check_auth()
        # Set plugin category. It is displayed in some skins as the name
        # of the current section.
        xbmcplugin.setPluginCategory(self.handle, self.lang(30011))
        # Set plugin content. It allows Kodi to select appropriate views
        # for this type of content.
        xbmcplugin.setContent(self.handle, 'videos')
        # Get the list of videos in the category.
        #videos = get_videos(category)
        # Iterate through videos.
        for video in self.con.my_play_channels:
            # Create a list item with a text label and a thumbnail image.
            list_item = xbmcgui.ListItem(label=video['name'])
            # Set additional info for the list item.
            # 'mediatype' is needed for skin to display info for this ListItem correctly.
            list_item.setInfo('video', {'title': video['name'],
                                        #'genre': 'Video',
                                        'mediatype': 'video'})
            # Set graphics (thumbnail, fanart, banner, poster, landscape etc.) for the list item.
            # Here we use the same image for all items for simplicity's sake.
            # In a real-life plugin you need to set each image accordingly.
            icon_url = 'http://tve-icons.han.telia.se/app/mh512b/ch_play_{channel}.png'.format(channel=video['id'])
            list_item.setArt({'thumb': icon_url, 'icon': icon_url, 'fanart': icon_url})
            # Set 'IsPlayable' property to 'true'.
            # This is mandatory for playable items!
            list_item.setProperty('IsPlayable', 'true')
            # Create a URL for a plugin recursive call.
            url = '{base}?action=list_play_item&play_item={item}&from_idx=0&to_idx={ps}'
            expanded_url = url.format(base=BASE_PLUGIN_URL, item=video['id'], ps=PAGE_SIZE)
            # Add the list item to a virtual Kodi folder.
            # is_folder = False means that this item won't open any sub-list.
            is_folder = True
            # Add our item to the Kodi virtual folder listing.
            xbmcplugin.addDirectoryItem(self.handle, expanded_url, list_item, is_folder)
        # Add a sort method for the virtual folder items (alphabetically, ignore articles)
        xbmcplugin.addSortMethod(self.handle, xbmcplugin.SORT_METHOD_LABEL_IGNORE_THE)
        # Finish creating a virtual folder.
        xbmcplugin.endOfDirectory(self.handle)

    def list_suggested_cat_content(self, ids, title):
        """
        List the suggested categories (list of ids) that is not a link to a store
        """
        cat_json = self.con.get_suggested_cat_content(ids)
        # Set plugin category. It is displayed in some skins as the name of the current section.
        xbmcplugin.setPluginCategory(self.handle, title)
        # Set plugin content. It allows Kodi to select appropriate views for this type of content.
        xbmcplugin.setContent(self.handle, 'videos')
        # create list of the result
        self.list_result(cat_json['Data']['directHits'])
        # Add a sort method for the virtual folder items (alphabetically, ignore articles)
        xbmcplugin.addSortMethod(self.handle, xbmcplugin.SORT_METHOD_LABEL_IGNORE_THE)
        # Finish creating a virtual folder.
        xbmcplugin.endOfDirectory(self.handle)

    def list_suggested_cat(self, cat_json):
        """
        """
        # Iterate through items.
        for item in cat_json:
            # Create a list item with a text label and a thumbnail image.
            list_item = xbmcgui.ListItem(label=item['title'])
            # Set additional info for the list item.
            # 'mediatype' is needed for skin to display info for this ListItem correctly.
            list_item.setInfo('video', {'title': item['title'],
                                        #'genre': 'Video',
                                        'mediatype': 'video'})
            # Set graphics (thumbnail, fanart, banner, poster, landscape etc.) for the list item.
            # Here we use the same image for all items for simplicity's sake.
            # In a real-life plugin you need to set each image accordingly.
            #icon_url = 'http://tve-icons.han.telia.se/app/mh512b/ch_play_{channel}.png'.format(channel=video['id'])
            #list_item.setArt({'thumb': icon_url, 'icon': icon_url, 'fanart': icon_url})
            # Set 'IsPlayable' property to 'true'.
            # This is mandatory for playable items!
            list_item.setProperty('IsPlayable', 'false')
            # Create a URL for a plugin recursive call. If a store-id is available use that, otherwise use special list
            try:
                store_id = item['content']['reference']['id']
            except KeyError:
                store_id = None
            if store_id:
                list_url = '{base}?action=list_play_item&play_item={sid}&from_idx=0&to_idx={ps}'
                url = list_url.format(base=BASE_PLUGIN_URL, sid=store_id, ps=PAGE_SIZE)
            else:
                cat_ids = []
                for cat in item['content']['items']:
                    cat_ids.append(cat['id'])
                cat_url = '{base}?action=list_suggested_cat_content&cat_ids={cat_ids}&cat_title={title}'
                title = quote(item['title'].encode('utf-8'))
                xbmc.log(title, xbmc.LOGDEBUG)
                url = cat_url.format(base=BASE_PLUGIN_URL, cat_ids=','.join(cat_ids), title=quote(title))
            # Add the list item to a virtual Kodi folder.
            # is_folder = False means that this item won't open any sub-list.
            is_folder = True
            # Add our item to the Kodi virtual folder listing.
            xbmcplugin.addDirectoryItem(self.handle, url, list_item, is_folder)

    def list_kids(self):
        """
        Create list of kids categories as seen on the webpage
        """
        kids_json = self.con.get_kids_categories()
        # Set plugin category. It is displayed in some skins as the name of the current section.
        xbmcplugin.setPluginCategory(self.handle, self.lang(30012))
        # Set plugin content. It allows Kodi to select appropriate views for this type of content.
        xbmcplugin.setContent(self.handle, 'videos')
        self.list_suggested_cat(kids_json)
        # Add a sort method for the virtual folder items (alphabetically, ignore articles)
        xbmcplugin.addSortMethod(self.handle, xbmcplugin.SORT_METHOD_LABEL_IGNORE_THE)
        # Finish creating a virtual folder.
        xbmcplugin.endOfDirectory(self.handle)

    def search(self):
        """
        Search for content
        """
        kb = xbmc.Keyboard('', 'Please enter the video title')
        kb.doModal()  # Onscreen keyboard appears
        if not kb.isConfirmed():
            return
        query = kb.getText()  # User input
        if not query:
            return
        # execute the search
        search_json = self.con.get_search_result(query)
        # Set plugin category. It is displayed in some skins as the name of the current section.
        xbmcplugin.setPluginCategory(self.handle, self.lang(30023))
        # Set plugin content. It allows Kodi to select appropriate views for this type of content.
        xbmcplugin.setContent(self.handle, 'videos')
        # create list of the result
        self.list_result(search_json['Data']['directHits'])
        # Add a sort method for the virtual folder items (alphabetically, ignore articles)
        xbmcplugin.addSortMethod(self.handle, xbmcplugin.SORT_METHOD_LABEL_IGNORE_THE)
        # Finish creating a virtual folder.
        xbmcplugin.endOfDirectory(self.handle)

    def frontpage(self):
        """
        Create the "frontpage" aka the root folder for this plugin
        """
        self.con.check_auth()
        # get the title string id. Use the values from the setting select. 30004 is at position 0.
        title_str_id = 30004 + self.con.country
        # Set plugin category. It is displayed in some skins as the name of the current section.
        xbmcplugin.setPluginCategory(self.handle, self.lang(title_str_id))
        # Add a folder for live channels
        li = xbmcgui.ListItem(self.lang(30010))
        live_tv_url = '{base}?action=list_live_channels'.format(base=BASE_PLUGIN_URL)
        xbmcplugin.addDirectoryItem(handle=self.handle, url=live_tv_url, listitem=li, isFolder=True)
        # Add a folder for Play / TV archive
        li = xbmcgui.ListItem(self.lang(30011))
        play_url = '{base}?action=list_play_channels'.format(base=BASE_PLUGIN_URL)
        xbmcplugin.addDirectoryItem(handle=self.handle, url=play_url, listitem=li, isFolder=True)
        # Add a folder for favorites
        li = xbmcgui.ListItem(self.lang(30013))
        play_url = '{base}?action=list_favorites'.format(base=BASE_PLUGIN_URL)
        xbmcplugin.addDirectoryItem(handle=self.handle, url=play_url, listitem=li, isFolder=True)
        # Add a folder for kids
        li = xbmcgui.ListItem(self.lang(30012))
        play_url = '{base}?action=list_kids'.format(base=BASE_PLUGIN_URL)
        xbmcplugin.addDirectoryItem(handle=self.handle, url=play_url, listitem=li, isFolder=True)
        # Add search
        li = xbmcgui.ListItem(self.lang(30022))
        li.setArt({'icon': 'DefaultAddonsSearch.png'})
        live_tv_url = '{base}?action=search'.format(base=BASE_PLUGIN_URL)
        xbmcplugin.addDirectoryItem(handle=self.handle, url=live_tv_url, listitem=li, isFolder=True)
        # get suggested categories
        welcome_json = self.con.get_frontpage_categories()
        # create items for categories
        self.list_suggested_cat(welcome_json)
        xbmcplugin.endOfDirectory(self.handle)

    def router(self, paramstring):
        """
        Router function that calls other functions
        depending on the provided paramstring
        :param paramstring: URL encoded plugin paramstring
        :type paramstring: str
        """
        # Parse a URL-encoded paramstring to the dictionary of
        # {<parameter>: <value>} elements
        params = dict(parse_qsl(paramstring))
        # Check the parameters passed to the plugin
        authentication_failed = False
        try:
            if params:
                if params['action'] == 'list_live_channels':
                    # Display the list of videos in a provided category.
                    self.list_live_channels()
                elif params['action'] == 'play_channel':
                    # Play a video from a provided URL.
                    self.con.play_item(params['channel'], 'CHANNEL')
                elif params['action'] == 'play_video':
                    # Play a video from a provided URL.
                    self.con.play_item(params['play_item'], 'CATCHUP')
                elif params['action'] == 'list_play_channels':
                    # Display the list of videos in a provided category.
                    self.list_play_channels()
                elif params['action'] == 'list_play_item':
                    self.list_play_items(params['play_item'], params['from_idx'], params['to_idx'])
                elif params['action'] == 'list_play_cat':
                    self.list_play_catagory(params['store_id'], params['cat_id'], params['from_idx'], params['to_idx'])
                elif params['action'] == 'list_seasons':
                    self.list_seasons(params['play_item'], params['seasons'])
                elif params['action'] == 'list_season':
                    self.list_season(params['play_item'], params['season'])
                elif params['action'] == 'list_kids':
                    self.list_kids()
                elif params['action'] == 'list_suggested_cat_content':
                    self.list_suggested_cat_content(params['cat_ids'], unquote(params['cat_title']))
                elif params['action'] == 'list_favorites':
                    self.list_favorites()
                elif params['action'] == 'search':
                    self.search()
                else:
                    # If the provided paramstring does not contain a supported action
                    # we raise an exception. This helps to catch coding errors,
                    # e.g. typos in action names.
                    raise ValueError('Invalid paramstring: {0}!'.format(paramstring))
            else:
                self.frontpage()
        except AuthenticationFailed as a:
            xbmc.log(str(a), xbmc.LOGDEBUG)
            # 114 is the error code for the error:
            #  'Could not entitle customer because of the maximum number of devices have been reached'
            if a.err_code == 114:
                xbmcgui.Dialog().ok(self.addon.getAddonInfo('name'), self.lang(30017), self.lang(30019))
            else:
                authentication_failed = True
        while authentication_failed:
            ret = xbmcgui.Dialog().yesno(self.addon.getAddonInfo('name'), self.lang(30017), self.lang(30018))
            authentication_failed = False
            if ret:
                # open the settings dialog if the user requested it
                xbmcaddon.Addon().openSettings()
                # authenticate and go to the root menu, otherwise we loop...
                try:
                    self.con.check_auth()
                    self.frontpage()
                except AuthenticationFailed:
                    authentication_failed = True
